//
// Created by Märt on 22-Dec-17.
// Algorithm based on Jos Stam's paper "Real-Time Fluid Dynamics for Games"
// http://www.dgp.toronto.edu/people/stam/reality/Research/pdf/GDC03.pdf
//

#include "FluidDynamics.h"

#define IX(i,j) ((i)+(N+2)*(j))

void Diffuse(int N, int b, float *x, float *x0, float diff, float dt)
{
    int i, j, k;
    float a = dt * diff * N * N;
    for (k = 0; k < 20; k++) {
        for (i = 1; i <= N; i++) {
            for (j = 1; j <= N; j++) {
                x[IX(i, j)] = (x0[IX(i, j)] + a * (x[IX(i - 1, j)] + x[IX(i + 1, j)] +
                                                   x[IX(i, j - 1)] + x[IX(i, j + 1)])) / (1 + 4 * a);
            }
        }
        SetBnd(N, b, x);
    }
}

void Advect(int N, int b, float *d, float *d0, float *u, float *v, float dt)
{
    int i, j, i0, j0, i1, j1;
    float x, y, s0, t0, s1, t1, dt0;
    dt0 = dt*N;
    for ( i=1 ; i<=N ; i++ ) {
        for ( j=1 ; j<=N ; j++ ) {
            x = i-dt0*u[IX(i,j)]; y = j-dt0*v[IX(i,j)];
            if (x<0.5) x=0.5; if (x>N+0.5) x=N+ 0.5; i0=(int)x; i1=i0+1;
            if (y<0.5) y=0.5; if (y>N+0.5) y=N+ 0.5; j0=(int)y; j1=j0+1;
            s1 = x-i0; s0 = 1-s1; t1 = y-j0; t0 = 1-t1;
            d[IX(i,j)] = s0*(t0*d0[IX(i0,j0)]+t1*d0[IX(i0,j1)]) + s1*(t0*d0[IX(i1,j0)]+t1*d0[IX(i1,j1)]);
        }
    }
    SetBnd(N, b, d);
}

void Project(int N, float *u, float *v, float *p, float *div)
{
    int i, j, k;
    float h;
    h = 1.0/N;
    for ( i=1 ; i<=N ; i++ ) {
        for ( j=1 ; j<=N ; j++ ) {
            div[IX(i,j)] = -0.5*h*(u[IX(i+1,j)]-u[IX(i-1,j)] +
                                   v[IX(i,j+1)]-v[IX(i,j-1)]);
            p[IX(i,j)] = 0;
        }
    }
    SetBnd(N, 0, div);
    SetBnd(N, 0, p);
    for ( k=0 ; k<20 ; k++ ) {
        for ( i=1 ; i<=N ; i++ ) {
            for ( j=1 ; j<=N ; j++ ) {
                p[IX(i,j)] = (div[IX(i,j)]+p[IX(i-1,j)]+p[IX(i+1,j)]+
                              p[IX(i,j-1)]+p[IX(i,j+1)])/4;
            }
        }
        SetBnd(N, 0, p);
    }
    for ( i=1 ; i<=N ; i++ ) {
        for ( j=1 ; j<=N ; j++ ) {
            u[IX(i,j)] -= 0.5*(p[IX(i+1,j)]-p[IX(i-1,j)])/h;
            v[IX(i,j)] -= 0.5*(p[IX(i,j+1)]-p[IX(i,j-1)])/h;
        }
    }
    SetBnd(N, 1, u);
    SetBnd(N, 2, v);
}

void SetBnd(int N, int b, float *x)
{
    int i;
    for(i = 1; i <= N; i++) {
        x[IX(0, i)] = b==1 ? -x[IX(1, i)] : x[IX(1, i)];
        x[IX(N+1, i)] = b==1 ? -x[IX(N, i)] : x[IX(N, i)];
        x[IX(i, 0)] = b==2 ? -x[IX(i, 1)] : x[IX(i, 1)];
        x[IX(i, N+1)] = b==2 ? -x[IX(i, N)] : x[IX(i, N)];
    }

    x[IX(0, 0)] = static_cast<float>(0.5 * (x[IX(1, 0)] + x[IX(0, 1)]));
    x[IX(0, N+1)] = static_cast<float>(0.5 * (x[IX(1, N + 1)] + x[IX(0, N)]));
    x[IX(N+1, 0)] = static_cast<float>(0.5 * (x[IX(N, 0)] + x[IX(N + 1, 1)]));
    x[IX(N+1, N+1)] = static_cast<float>(0.5 * (x[IX(N, N + 1)] + x[IX(N + 1, N)]));
}