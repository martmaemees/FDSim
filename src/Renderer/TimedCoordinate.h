//
// Created by Märt on 23-Dec-17.
//

#pragma once


#include <cstddef>

struct TimedCoordinate
{
public:
    int x, y;
    double time;

    TimedCoordinate(int x, int y)
    {
        this->x = x;
        this->y = y;
        this->time = 0.0;
    }

    bool operator==(const TimedCoordinate &rhs) const
    {
        return x == rhs.x &&
               y == rhs.y;
    }

    bool operator!=(const TimedCoordinate &rhs) const
    {
        return !(rhs == *this);
    }
};
