//
// Created by Märt on 23-Dec-17.
//

#include <string>
#include <glad/glad.h>
#include <fstream>
#include <iostream>
#include <sstream>
#include "Shader.h"

/// Compiles the shader program using source from the paths given and stores the program ID.
Shader::Shader(const char *vertexPath, const char *fragmentPath)
{
    std::string vertCode, fragCode;
    std::ifstream vertFile, fragFile;
    vertFile.exceptions(std::ifstream::failbit | std::ifstream::badbit);
    fragFile.exceptions(std::ifstream::failbit | std::ifstream::badbit);

    // Reads the content of the given files into strings.
    try {
        vertFile.open(vertexPath);
        std::stringstream stream;
        stream << vertFile.rdbuf();
        vertFile.close();
        vertCode = stream.str();
    } catch (std::ifstream::failure e) {
        std::cerr << "ERROR::SHADER::VERTEX::FILE_NOT_SUCCESSFULLY_READ" << std::endl;
    }

    try {
        fragFile.open(fragmentPath);
        std::stringstream stream;
        stream << fragFile.rdbuf();
        fragFile.close();
        fragCode = stream.str();
    } catch (std::ifstream::failure e) {
        std::cerr << "ERROR::SHADER::FRAGMENT::FILE_NOT_SUCCESSFULLY_READ" << std::endl;
    }

    const char *vShaderCode = vertCode.c_str();
    const char *fShaderCode = fragCode.c_str();

    unsigned int vertID, fragID;
    int success;
    char infoLog[512];

    // Compiles Shaders and outputs any errors.
    vertID = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(vertID, 1, &vShaderCode, NULL);
    glCompileShader(vertID);

    glGetShaderiv(vertID, GL_COMPILE_STATUS, &success);
    if (!success) {
        glGetShaderInfoLog(vertID, 512, NULL, infoLog);
        std::cout << "ERROR::SHADER::VERTEX::COMPILATION_FAILED\n" << infoLog << std::endl;
    }

    fragID = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(fragID, 1, &fShaderCode, NULL);
    glCompileShader(fragID);

    glGetShaderiv(fragID, GL_COMPILE_STATUS, &success);
    if (!success) {
        glGetShaderInfoLog(fragID, 512, NULL, infoLog);
        std::cout << "ERROR::SHADER::FRAGMENT::COMPILATION_FAILED\n" << infoLog << std::endl;
    }

    // Links shaders into a program and outputs any linking errors.
    ID = glCreateProgram();
    glAttachShader(ID, vertID);
    glAttachShader(ID, fragID);
    glLinkProgram(ID);

    glGetProgramiv(ID, GL_LINK_STATUS, &success);
    if(!success) {
        glGetProgramInfoLog(ID, 512, NULL, infoLog);
        std::cout << "ERROR::SHADER::PROGRAM::LINKING_FAILED\n" << infoLog << std::endl;
    }

    // Gets rid of the separate shaders, since they are no longer needed.
    glDeleteShader(vertID);
    glDeleteShader(fragID);
}

Shader::~Shader()
{
    glDeleteProgram(ID);
}

void Shader::Use()
{
    glUseProgram(ID);
}

unsigned int Shader::GetID()
{
    return ID;
}

