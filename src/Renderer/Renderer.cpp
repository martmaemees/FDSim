//
// Created by Märt on 23-Dec-17.
//

#include <glad/glad.h>
#include <iostream>
#include <sstream>
#include <iomanip>
#include "Renderer.h"
#include "../Math.h"
#include <chrono>
#include <cmath>

Renderer::Renderer(unsigned int size, unsigned int windowSize, std::string name)
{
    matrix = new FluidMatrix(size);
    window = new Window(windowSize, windowSize, name);
    this->size = size;
    this->windowSize = windowSize;

    glfwSetCursorPosCallback(window->GetWindow(), Input::CursorPositionCallback);
    glfwSetMouseButtonCallback(window->GetWindow(), Input::MouseButtonCallback);

    vertexPositions = new float[size * size * 3];
    triangleIndices = new unsigned int[(size - 1) * (size - 1) * 2 * 3];
    density = new float[size * size];

    // Sets up the vertices for the quad to display.
    for (int y = 0; y < size; y++) {
        for (int x = 0; x < size; x++) {
            vertexPositions[(y * size + x) * 3] = (static_cast<float>(x) / (size - 1)) * 2 - 1;
            vertexPositions[(y * size + x) * 3 + 1] = (static_cast<float>(y) / (size - 1)) * 2 - 1;
            vertexPositions[(y * size + x) * 3 + 2] = 0.0f;
        }
    }
    int ti = 0;
    for (int y = 0; y < size - 1; y++) {
        for (int x = 0; x < size - 1; x++) {
            triangleIndices[ti] = x + y * size;
            triangleIndices[ti + 1] = triangleIndices[ti + 4] = (x + 1) + y * size;
            triangleIndices[ti + 2] = triangleIndices[ti + 3] = x + (y + 1) * size;
            triangleIndices[ti + 5] = (x + 1) + (y + 1) * size;
            ti += 6;
        }
    }

    shader = new Shader("./Shaders/shader.vert", "./Shaders/blueShader.frag");
//    shader = new Shader("Shaders/uvShader.vert", "Shaders/uvShader.frag");
    lastTime = static_cast<float>(glfwGetTime());
}

Renderer::~Renderer()
{
    delete matrix;
    delete window;
    delete shader;
    delete[] vertexPositions, triangleIndices, density;

    glDeleteVertexArrays(1, &vao);
    glDeleteBuffers(1, &vbo);
    glDeleteBuffers(1, &ebo);
}

void Renderer::Init()
{
    shader->Use();

    glGenVertexArrays(1, &vao);
    glGenBuffers(1, &vbo);
    glGenBuffers(1, &ebo);

    // Buffers the vertex positions and the initial density of the matrix to the gpu.
    glBindVertexArray(vao);
    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glBufferData(GL_ARRAY_BUFFER, sizeof(float) * (size * size * 3 + (size) * (size)), NULL, GL_STREAM_DRAW);
    // Vertex Positions
    glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(float) * (size * size * 3), vertexPositions);
    // Vertex Densities
    glBufferSubData(GL_ARRAY_BUFFER, sizeof(float) * (size * size * 3), sizeof(float) * (size) * (size),
                    matrix->GetDens());

    // Sets the vertex attribute pointers
    glEnableVertexAttribArray(0); // Vertex position attribute
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(float) * 3, 0);
    glEnableVertexAttribArray(1); // Density attribute
    glVertexAttribPointer(1, 1, GL_FLOAT, GL_FALSE, sizeof(float), (void *) (sizeof(float) * size * size * 3));

    // Buffers the triangle indices to the gpu.
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(int) * (size - 1) * (size - 1) * 2 * 3, triangleIndices,
                 GL_STATIC_DRAW);

    glBindVertexArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}

void Renderer::Update()
{
    float deltaTime = static_cast<float>(glfwGetTime()) - lastTime;
    lastTime = static_cast<float>(glfwGetTime());

    matrix->ResetDensitySources();
    matrix->ResetVelocitySources();

    // Loops through all density sources and deletes them if their time has exceeded the limit, otherwise adds a source to the spot.
    for (auto tc = densitySources.begin(); tc != densitySources.end();) {
        tc->time += deltaTime;
        if (tc->time >= densSourceLifetime) {
            tc = densitySources.erase(tc);
        } else {
            matrix->SetDensitySource(tc->x, tc->y, matrix->GetDensAt(tc->x, tc->y) + densitySourceStrength);
            ++tc;
        }
    }

    // Loops through all velocity sources and deletes them if their time has exceeded the limit, otherwise adds a source to the surrounding spots.
    for (auto tc = velocitySources.begin(); tc != velocitySources.end();) {
        tc->time += deltaTime;
        if (tc->time >= velSourceLifetime) {
            tc = velocitySources.erase(tc);
        } else {
            matrix->SetVelocitySource(tc->x + 1, tc->y, matrix->GetVxAt(tc->x + 1, tc->y) + velocitySourceStrength,
                                      matrix->GetVyAt(tc->x + 1, tc->y));
            matrix->SetVelocitySource(tc->x - 1, tc->y, matrix->GetVxAt(tc->x - 1, tc->y) - velocitySourceStrength,
                                      matrix->GetVyAt(tc->x - 1, tc->y));
            matrix->SetVelocitySource(tc->x, tc->y + 1, matrix->GetVxAt(tc->x, tc->y + 1),
                                      matrix->GetVyAt(tc->x, tc->y + 1) + velocitySourceStrength);
            matrix->SetVelocitySource(tc->x, tc->y - 1, matrix->GetVxAt(tc->x, tc->y - 1),
                                      matrix->GetVyAt(tc->x, tc->y - 1) - velocitySourceStrength);
            ++tc;
        }
    }

    matrix->TimeStep(deltaTime * timeMultiplier);
    UpdateDensityData();
}

void Renderer::UpdateDensityData()
{
    glBindVertexArray(vao);
    glBindBuffer(GL_ARRAY_BUFFER, vbo);


    const float *m = matrix->GetDens();
    // Gets the correct size matrix, getting rid of the edges of the matrix given by the FluidMatrix.
    for (int y = 0; y < size; y++) {
        for (int x = 0; x < size; x++) {
            density[x + size * y] = m[(x + 1) +
                                      (1 + y) * (size + 2)]; // size+2 -> the real density matrix width is size+2.
        }
    }
    // Buffers the density array into the gpu again.
    glBufferSubData(GL_ARRAY_BUFFER, sizeof(float) * (size * size * 3), sizeof(float) * size * size, density);

    glBindVertexArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
}

void Renderer::Render()
{
    glClear(GL_COLOR_BUFFER_BIT);

    shader->Use();
    glBindVertexArray(vao);
    glDrawElements(GL_TRIANGLES, (size - 1) * (size - 1) * 2 * 3, GL_UNSIGNED_INT, 0);

    glfwSwapBuffers(window->GetWindow());
}

/// Runs Init() and then starts the gameplay loop.
void Renderer::Start()
{
    Init();
    while (!glfwWindowShouldClose(window->GetWindow())) {
        HandleInput();
        Update();
        Render();
        glfwPollEvents();
    }
}

FluidMatrix *Renderer::GetMatrix()
{
    return matrix;
}

void Renderer::HandleInput()
{
    if (input.IsLeftPressed() || input.IsRightPressed()) {
        // Calculates the point in the matrix the mouse is closest to.
        int x = static_cast<int>(std::round(
                Math::clamp(input.getCursorPosX(), 0, windowSize) * (static_cast<float>(size) / windowSize)));
        int y = static_cast<int>(std::round(windowSize - Math::clamp(input.getCursorPosY(), 0, windowSize)) *
                                 (static_cast<float>(size) / windowSize));
        if (input.IsLeftPressed())
            densitySources.push_back(TimedCoordinate(x, y));
        if (input.IsRightPressed())
            velocitySources.push_back(TimedCoordinate(x, y));
    }
}
