//
// Created by Märt on 23-Dec-17.
//

#pragma once

class Shader
{
public:
    Shader(const char *vertexPath, const char *fragmentPath);
    Shader();
    ~Shader();
    void Use();

    unsigned int GetID();

private:
    GLuint ID;
};
