//
// Created by Märt on 23-Dec-17.
//

#pragma once

#include <GLFW/glfw3.h>
#include <string>

class Window
{
public:
    Window(unsigned int width, unsigned int height, std::string name);
    ~Window();

    GLFWwindow *GetWindow() const
    {
        return window;
    }

private:
    GLFWwindow* window;
    GLuint width, height;
    std::string name;
};
