//
// Created by Märt on 23-Dec-17.
//

#include <glad/glad.h>
#include "Window.h"
#include <iostream>

Window::Window(unsigned int width, unsigned int height, std::string name)
{
    this->width = width;
    this->height = height;
    this->name = name;

    if(!glfwInit()) {
        std::cerr << "Failed to initialize GLFW!" << std::endl;
        return;
    }
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);

    window = glfwCreateWindow(this->width, this->height, this->name.c_str(), nullptr, nullptr);
    if(window == nullptr)
    {
        std::cerr << "Failed to create a window!" << std::endl;
        glfwTerminate();
        return;
    }
    glfwMakeContextCurrent(window);

    if(!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
    {
        std::cerr << "Failed to initialize GLAD" << std::endl;
        glfwTerminate();
        return;
    }

    glViewport(0, 0, this->width, this->height);
//    glEnable(GL_CULL_FACE);
    glFrontFace(GL_CCW);
    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

    gladLoadGL();
}

Window::~Window()
{
    glfwTerminate();
}
