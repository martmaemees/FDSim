
//
// Created by Märt on 23-Dec-17.
//

#pragma once

#include "../FluidDynamics/FluidMatrix.h"
#include "Window.h"
#include "Shader.h"
#include "TimedCoordinate.h"
#include "Input.h"
#include <vector>

class Renderer
{
public:
    Renderer(unsigned int size, unsigned int windowSize, std::string name);
    ~Renderer();

    void Start();
    FluidMatrix *GetMatrix();

private:
    constexpr static float timeMultiplier = 1.0f;
    constexpr static float densitySourceStrength = 20.0f;
    constexpr static float velocitySourceStrength = -20.0f;
    constexpr static float densSourceLifetime = 2.0f;
    constexpr static float velSourceLifetime = 0.5f;

    int size;
    int windowSize;
    FluidMatrix *matrix;
    Window *window;
    Shader *shader;
    Input &input = Input::GetInstance();
    float *vertexPositions;
    unsigned int *triangleIndices;
    float *density;
    float lastTime = 0.0;
    std::vector<TimedCoordinate> densitySources;
    std::vector<TimedCoordinate> velocitySources;

    GLuint vao, vbo, ebo;

    void Init();
    void Update();
    void Render();
    void UpdateDensityData();
    void HandleInput();
};
