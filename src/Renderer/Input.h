//
// Created by Märt on 23-Dec-17.
//

#pragma once


#include <GLFW/glfw3.h>

class Input
{
public:
    static Input &GetInstance()
    {
        static Input instance;
        return instance;
    }

    bool IsLeftPressed()
    { return leftPressed; }

    bool IsRightPressed()
    { return rightPressed; }

    double getCursorPosX()
    { return xPos; }

    double getCursorPosY()
    { return yPos; }

    double xPos, yPos;
    bool leftPressed = false;
    bool rightPressed = false;

    static void MouseButtonCallback(GLFWwindow *window, int button, int action, int mods)
    {
        if (button == GLFW_MOUSE_BUTTON_LEFT) {
            Input::GetInstance().leftPressed = action == GLFW_PRESS;
        } else if (button == GLFW_MOUSE_BUTTON_RIGHT) {
            Input::GetInstance().rightPressed = action == GLFW_PRESS;
        }
    };

    static void CursorPositionCallback(GLFWwindow *window, double x, double y)
    {
        Input::GetInstance().xPos = x;
        Input::GetInstance().yPos = y;
    };

private:
    Input()
    {} // Private constructor to allow only 1 singleton instance
    Input(Input const &); // Prevent copies?
    void operator=(Input const &); // Prevent assignments

};

