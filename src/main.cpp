#include <iostream>
#include "Renderer/Renderer.h"

int main()
{
    constexpr int GRID_SIZE = 200;
    constexpr int WINDOW_FACTOR = 3;
    Renderer renderer(GRID_SIZE, GRID_SIZE*WINDOW_FACTOR, "FDSim");
    renderer.Start();

    return 0;
}