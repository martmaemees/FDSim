//
// Created by Märt on 23-Dec-17.
//
#pragma once

namespace Math
{
/// Clamps the argument between minimum and maximum and returns it.
int clamp(int x, int min, int max)
{
    if (x > max)
        return max;
    if (x < min)
        return min;
    return x;
}
}
