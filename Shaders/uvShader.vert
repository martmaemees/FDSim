#version 460 core

layout (location = 0) in vec3 position;

out vec3 fColor;

void main() {
    gl_Position = vec4(position, 1.0);
    fColor = vec3(gl_VertexID / 256.0);
}