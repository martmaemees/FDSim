#version 330 core

in float fDensity;

out vec4 color;

void main() {
    float d = fDensity*1.0;
    color = vec4(d-2.0, d-1.0, d, 1.0);
//    color = vec4(1.0, 1.0, 1.0, 1.0);
}