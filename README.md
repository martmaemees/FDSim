# Fluid Dynamics Simulator

## Introduction

This is a Fluid Dynamics Simulator written in C++, using OpenGL with GLFW and GLAD for rendering.
This is a rewrite of my school project [CoralKeyboardGL](https://gitlab.com/martmaemees/CoralKeyboardGL).

The simulation algorithm for the fluid dynamics is from [Real-Time Fluid Dynamics for Games by Jos Stam](http://www.dgp.toronto.edu/people/stam/reality/Research/pdf/GDC03.pdf)

[Video of the simulator](http://kodu.ut.ee/~mmaemees/media/FDSim%20Showcase.gif)

## Running

<b>Windows: </b>
Download the [Latest Build](http://kodu.ut.ee/~mmaemees/downloads/FDSim.zip), unpack the files into the same folder and run the .exe file.